% Calculates the expected difference between the posterior and prior
% estimates of the quantity of interest, based on simulations


function ret_val = policy_kg(i, mu0, Sigma0, sigma2W, ~, num_mc_steps, ...
    qoi, qoi_dist)

    ret_val = zeros(size(i));
    sigmaW = sqrt(sigma2W);
    prior_qoi = qoi(mu0);

    % make sure this is a column vector
    assert(size(mu0, 2) == 1)

    % MC Loop
    for k=1:num_mc_steps
        % generate truth
        mu = mvnrnd(mu0, Sigma0);

        % iterate over potential  experiments (indexed by i vector)
        for j=1:length(i)
            ij = i(j);

            % simulate independent measurements;
            obs = mu(ij) + sigmaW*randn();
            
            % bayes update
            [mu_n, ~] = update_lookuptable(mu0, Sigma0, sigma2W, ij, ...
                    obs, false);

            % add sampled KG difference
            ret_val(j) = ret_val(j) + qoi_dist(qoi(mu_n), prior_qoi);
        end
    end

    % take average
    ret_val = ret_val/ num_mc_steps;
end

