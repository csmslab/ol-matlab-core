% Simulates E[D_kl(p^{n+1}, p^n) | x_i]

function ret_val = policy_klla(i, mu0, Sigma0, sigma2W, ~, num_mc_steps)
    ret_val = zeros(size(i));
    sigmaW = sqrt(sigma2W);

    % make sure this is a column vector
    assert(size(mu0, 2) == 1)

    % MC Loop
    for k=1:num_mc_steps
        % generate truth
        mu = mvnrnd(mu0, Sigma0);
        
        % iterate over potential  experiments (indexed by i vector)
        for j=1:length(i)
            ij = i(j);
            
            % simulate independent measurements;
            obs = mu(ij) + sigmaW*randn();
            
            % bayes update
            [mu_n, Sigma_n] = update_lookuptable(mu0, Sigma0, sigma2W, ...
                ij, obs, true);
                
             % add sampled KL Divergence
            ret_val(j) = ret_val(j) + kldiv(mu0, Sigma0, mu_n, Sigma_n);
        end
    end
    
    % take average
    ret_val = ret_val/ num_mc_steps;   
end

function ret_val = kldiv(mu1, Sigma1, mu2, Sigma2)
    %tr = trace(Sigma2\Sigma1);
    %pr = Sigma2\(mu2-mu1);
    %ret_val = 0.5*(log(det(Sigma2)/det(Sigma1)) + tr + ...
    %     (mu2-mu1)'*pr - length(mu1));
    
    k = length(mu1);
    tmp = Sigma2\Sigma1;
    ret_val = 0.5*(trace(tmp)+(mu2-mu1)'*(Sigma2\(mu2-mu1))-k-log(det(tmp)));
end
