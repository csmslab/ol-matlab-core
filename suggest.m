function i_max = suggest(mu, Sigma, sigma2W, policy_fnc, n)
    i = 1:length(mu);
    nu = policy_fnc(i, mu, Sigma, sigma2W, n);
    [~, i_max] = max(nu);
end

