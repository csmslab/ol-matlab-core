% Finds the peak for a 1D response function

function ret_val = qoi_extrema_1D(mu, n, X)

[~, ret_val] = findpeaks(mu);

ret_val = sort(ret_val);
if(length(ret_val) > n)
    ret_val = ret_val(1:n);
end

ret_val = X(ret_val);

end



