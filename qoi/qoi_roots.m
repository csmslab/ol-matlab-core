% returns the indices of the num_extrema smallest derivative values in
% absolute value

function ret_val = qoi_roots( mu, num_roots )

[~, ret_val] = sort(abs(mu));

ret_val = sort(ret_val(1:num_roots));

end



