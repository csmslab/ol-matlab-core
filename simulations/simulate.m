% Performs optimal learning simulations, looping over simulations, policies
% and noise levels.

function [truth, observations, mu, Sigma, choices, prior_samples] = simulate(num_sims, ... 
    num_samples_for_priors, num_experiments, policies, sigmaw, ...
    generate_truth, X)

% Preallocate arrays
truth = zeros(num_sims, length(X));
observations = zeros(num_sims, length(sigmaw), num_experiments, length(X));
mu = zeros(num_sims, length(sigmaw), length(policies), ...
    num_experiments, length(X));
Sigma = zeros(num_sims, length(sigmaw), length(policies), ...
    num_experiments, length(X), length(X));
choices = zeros(num_sims, length(sigmaw), length(policies), ...
    num_experiments);

sigma2w = sigmaw.*sigmaw;

% generate prior by sampling
prior_samples = zeros(num_samples_for_priors, length(X));
for i=1:num_samples_for_priors
    prior_samples(i,:) = generate_truth(X);
end
mu_0 = transpose(mean(prior_samples));
Sigma_0 = cov(prior_samples) + eye(length(X))*1e-6;

% generate truth and noisy measurements
for sim_idx = 1:num_sims
    % generate true response function, making sure target is within bounds
    truth(sim_idx,:) = generate_truth(X);    
    
    for noise_idx = 1:length(sigmaw)
        observations(sim_idx, noise_idx, :, :) = ...
            repmat(truth(sim_idx,:), num_experiments, 1) + ...
            sigmaw(noise_idx)*randn(num_experiments, length(X));
    end
end

% parallel simulation runs

parfor sim_idx = 1:num_sims
    
    obs_for_sim = squeeze(observations(sim_idx, :, :, :));
      
    % preallocate some arrays local to specific simulation iteration
    mu_for_sim = zeros(length(sigmaw), length(policies), ...
        num_experiments, length(X));
    Sigma_for_sim = zeros(length(sigmaw), length(policies), ...
        num_experiments, length(X), length(X));
    choices_for_sim = zeros(length(sigmaw), length(policies), ...
        num_experiments);
         
    % iterate over noise levels
    for noise_idx = 1:length(sigmaw)
                        
        % iterate over policies
        for policy_idx = 1:length(policies)
            policy = policies{policy_idx};
            
            % set current belief to prior belief
            mu_n = mu_0;
            Sigma_n = Sigma_0;
            
            % perform measurements
            for n = 1:num_experiments
                % save estimation
                mu_for_sim(noise_idx, policy_idx, n, :) = mu_n;
                Sigma_for_sim(noise_idx, policy_idx, n, :, :) = Sigma_n;
                                                                                   
                % select experiment
                i_n = suggest(mu_n, Sigma_n, sigma2w(noise_idx), policy, n);
                choices_for_sim(noise_idx, policy_idx, n) = i_n;
                
                % noisy measurement
                y_np1 = obs_for_sim(noise_idx, n, i_n);
                
                % bayesian update
                [mu_n, Sigma_n] = update_lookuptable(mu_n, Sigma_n, ...
                    sigma2w(noise_idx), i_n, y_np1);
            end
            
        end
        
    end
    
    % save local arrays to global arrays
    mu(sim_idx, :, :, :, :) = mu_for_sim;
    Sigma(sim_idx, :, :, :, :, :) = Sigma_for_sim;
    choices(sim_idx, :, :, :) = choices_for_sim;
end


