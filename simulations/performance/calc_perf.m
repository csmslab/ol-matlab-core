% Calculates the performance metric between several simulations
% with mu and truth holding simulation results.

function perf = calc_perf( mu, truth, perf_metric )

num_sims = size(mu, 1);
num_noise = size(mu, 2);
num_policies = size(mu, 3);
num_experiments = size(mu, 4);

perf = zeros(num_sims, num_noise, num_policies, num_experiments);


for sim_idx = 1:num_sims
    truth_s = truth(sim_idx, :)';
        
    for noise_idx = 1:num_noise
        for policy_idx = 1:num_policies
            for n=1:num_experiments
                mu_n = squeeze(mu(sim_idx, noise_idx, policy_idx, n,:));
                perf(sim_idx, noise_idx, policy_idx, n) = ...
                    perf_metric(mu_n, truth_s);
            end
        end
    end
end


end

