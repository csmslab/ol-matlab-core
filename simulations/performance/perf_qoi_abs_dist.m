% absolute performance based on qoi distance from truth, using only qoi_dist metric
function ret_val = perf_qoi_abs_dist( mu, truth, qoi, qoi_dist)
    ret_val = qoi_dist(qoi(mu), qoi(truth));
end
