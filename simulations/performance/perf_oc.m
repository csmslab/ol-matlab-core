% Opportunity cost - for optimization
function ret_val = perf_oc( mu, truth, qoi, qoi_norm )
    [~, i] = max(mu);
    val = max(truth);
    ret_val = abs((val - truth(i))/val);
end
