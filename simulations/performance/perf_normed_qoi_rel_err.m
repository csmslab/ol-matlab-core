% relative performance based on norm for those QOI that come equipped with a norm
function ret_val = perf_normed_qoi_rel_err( mu, truth, qoi, qoi_norm )
    ret_val = qoi_norm(qoi(mu) - qoi(truth))/qoi_norm(truth);
end
