function  F = animate_beliefs(X, mu, Sigma, obs, choices, truth, ...
    num_to_show, prefix)

figure();
XX = min(X):((max(X)-min(X))/1000):max(X);
method = 'PCHIP';

flat_obs = zeros(1, size(mu, 1));
for n=1:size(mu, 1)
    flat_obs(n) = obs(n, choices(n));
end


F(size(mu, 1)) = struct('cdata',[],'colormap',[]);
for n=1:(size(mu, 1)+num_to_show)
    
    if n < num_to_show
        I = n:-1:1;
        nn = n;
    elseif(n > size(mu,1))
        nn = size(mu, 1);
        I = size(mu,1):-1:(size(mu,1) - (num_to_show - (n - size(mu,1))));
    else
        nn = n;
        I = n:-1:(n-num_to_show+1);
    end
    
    clf;
    subplot(3,1,[1,2]);
    
    for idx=1:length(I)
        if(idx==1)
            width = 3;
        else
            width = 3;
        end
        
        if(idx==1)
            opacity = 1;
        else
            opacity = 0.25*(length(I)-idx+1)/length(I);
        end
        
        % plot envelope
        m = interp1(X, mu(I(idx),:), XX, method);
        d = interp1(X, diag(squeeze(Sigma(I(idx), :,:))), XX, method);
        mm = m+d;
        mm = mm(end:-1:1);
        h = fill([XX XX(end:-1:1)], [m-d mm], [0, 0.45, 0.74]);
        set(h, 'FaceAlpha', 0.25*opacity);
        set(h, 'EdgeColor', 'none');
        hold on;
        
        % plot mean
        h = plot_fnc(XX, m);
        set(h, 'LineWidth', width);
        set(h, 'Color', [0, 0.45, 0.74, opacity]);         
    end
    
    % plot truth
    h = plot_fnc(XX, interp1(X, truth, XX, method));
    set(h, 'LineWidth', 3);
    set(h, 'Color', [0.85, 0.33, 0.1, 1]);

    % plot prior
    h = plot_fnc(XX, interp1(X, mu(1,:), XX, method));
    set(h, 'LineWidth', 3);
    set(h, 'Color', [0.491 0.18, 0.56, 1]);
      
    % plot observations
    h = scatter(X(choices(1:nn)), flat_obs(1:nn));
    set(h, 'MarkerFaceColor', [0.85 0.33 0.1]);
    set(h, 'MarkerEdgeColor', [0.85 0.33 0.1]);
    set(h, 'MarkerFaceAlpha', 1);
    set(h, 'MarkerEdgeAlpha', 0.0);
    set(gca, 'xlim', [min(X)-0.5, max(X)+0.5]);
    ylabel('Response function');
    grid on;
    set(gca, 'FontSize', 16);
    hold off;
    
    subplot(3,1,3)
    dx = 0.5*mean(X(2:end)-X(1:end-1));
    histogram(X(choices(1:nn)), [X-dx, X(end)+dx]...
        , 'EdgeAlpha', 0.1);
    set(gca, 'xlim', [min(X)-0.5, max(X)+0.5]);
    xlabel('X');
    ylabel('Frequency');
    grid on;
    set(gca, 'FontSize', 16);
    
    if(exist('prefix', 'var') ~= 0)
        saveas(gcf, sprintf('%s.%03d.pdf', prefix, n));
    else
        drawnow;
    end
end

end

