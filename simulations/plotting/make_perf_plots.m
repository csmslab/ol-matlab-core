function make_perf_plots( perf, legend_labels, title_label, prefix, label)

    % aggregate performance over several simulations
    mean_perf = squeeze(mean(perf(:,:,:), 1));
    %p25_perf = squeeze(prctile(oc(:,:,:), 25, 1));
    %p50_perf = squeeze(prctile(oc(:,:,:), 50, 1));
    %p75_perf = squeeze(prctile(oc(:,:,:), 75, 1));
   
    figure();
    plot(mean_perf', 'LineWidth', 2);
    %hold on;
    %set(gca, 'ColorOrderIndex', 1);
    %plot(p50_perf', 'LineWidth', 1, 'LineStyle', '--');
    %set(gca, 'ColorOrderIndex', 1);
    %plot(p25_perf', 'LineWidth', 1, 'LineStyle', ':');
    %set(gca, 'ColorOrderIndex', 1);
    %plot(p75_perf', 'LineWidth', 1, 'LineStyle', ':');
    
    set(gca, 'FontSize', 18);
    legend(legend_labels, 'FontSize', 14);
    title(title_label, 'FontSize', 18);
    xlabel('# Experiments', 'FontSize', 18);
    ylabel(label, 'FontSize', 18);
    grid on;  
    saveas(gcf, sprintf('%s.pdf', prefix));
end

