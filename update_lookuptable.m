% Performs bayesian update assuming normal prior and noise, after 
% observing outcome y for alternative i, with noise variance sigma2W

function [ mu, Sigma ] = update_lookuptable( mu0, Sigma0, sigma2W, i, y, computeSigma0)

% make sure we have a column vector
assert(size(mu0, 1) > 1)
assert(size(mu0, 2) == 1);

if(exist('computeSigma0') == 0)
    computeSigma0 = true;
end

gamma = 1/(sigma2W + Sigma0(i,i));
mu = mu0 + (y - mu0(i))*gamma*Sigma0(:, i);
if(computeSigma0)
    Sigma = Sigma0 - gamma * (Sigma0(:,i)*Sigma0(i,:));
else
    Sigma = 0;
end

assert(size(mu,1) == size(mu0,1));
assert(size(mu,2) == size(mu0,2));

end


